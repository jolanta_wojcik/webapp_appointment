create table patient(
	id int NOT NULL AUTO_INCREMENT,
	name varchar(255) not null,
	surname varchar(255) not null, 
	dateOfBirth date not null,
	primary key(id)
);
insert into patient (name, surname, dateOfBirth) values ('Jan', 'Kowaslki', '07-04-1990');

CREATE TABLE `appoitment`.`appoitment` (
	id int NOT NULL AUTO_INCREMENT,
	dateOfApp date not null,
	price integer not null,
	reasone varchar(255) not null, 
	patient_id int not null,
	primary key(id),
	foreign key (patient_id) references patient(id)
);
insert into patient (dateOfApp, price , reason, patient_id) values (10-03-2017, 0, 'headache', 1);

create table user_account(
	id int NOT NULL AUTO_INCREMENT,
	user_name varchar(255) not null,
	password varchar(255) not null,
	primary key(id)
);
insert into user_account (user_name, password) values ('admin', 'admin');

