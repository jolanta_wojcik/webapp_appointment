<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User page</title>
</head>
<body>
	<div id="loginForm"
		style="background-color: #EEEEEE; margin-left: auto;">
		<c:if test="${appUser!=null}">
			<form action="doLogout" method="post">
				<table style="margin-left: auto;">
					<tr>
						<td>Witaj: ${appUser.name}</td>
						<td><input type="submit" value="Wyloguj" /></td>
					</tr>
				</table>
			</form>
		</c:if>
	</div>
	<div id="addForm"
			style="width: 550px; margin-left: auto; margin-right: auto; background-color: #EEEEEE; border: 1px solid">
			<form action="addAppoitment" method="post">
				<table style="width: 500px; margin-left: auto; margin-right: auto;">
					<tr>
						<td>Wprowadz termin wizyty</td>
						<td><input type="text" name="dateOfApp" /></td>
					</tr>
					<tr>
						<td>Wpisz cene</td>
						<td><input type="text" name="price" /></td>
					</tr>
					<tr>
						<td>Wpisz przyczyne</td>
						<td><input type="text" name="reason" /></td>
					</tr>
					<tr>
						<td colspan="2" align="center"><input type="submit"
							value="Dodaj wizyte" /></td>
					</tr>
				</table>
			</form>
		</div>

		<div id="resultPage"
			style="width: 550px; margin-left: auto; margin-right: auto; background-color: #EEEEEE; border: 1px solid">
			<table style="width: 550px; margin-left: auto; margin-right: auto;"
				border="1" cellspacing="0" cellpadding="0">
				<tr bgcolor="orange">
						<td>Termin</td>
						<td>Cena</td>
						<td>Przyczyna</td>
				</tr>
					<c:forEach items="${appoitmentsList}" var="a">
					<tr>
						<td><fmt:formatDate value="${a.date}" pattern="yyyy-MM-dd" /></td>
						<td>${a.price}</td>
						<td>${a.reason}</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
</body>
</html>