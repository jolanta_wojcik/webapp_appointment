<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin page</title>
</head>
<body>
	<div id="loginForm"
		style="background-color: #EEEEEE; margin-left: auto;">
		<c:if test="${appUser!=null}">
			<form action="doLogout" method="post">
				<table style="margin-left: auto;">
					<tr>
						<td>Witaj: ${appUser.name}</td>
						<td><input type="submit" value="Wyloguj" /></td>
					</tr>
				</table>
			</form>
		</c:if>
	</div>
	<div id="resultPage"
		style="width: 550px; margin-left: auto; margin-right: auto; background-color: #EEEEEE; border: 1px solid">
		<table style="width: 550px; margin-left: auto; margin-right: auto;"
			border="1" cellspacing="0" cellpadding="0">
			<tr bgcolor="orange">
				<td>Imie</td>
				<td
					onclick="document.location.href='/webapp_appoitment/?sortBy=surname'"
					onmouseover="this.style.cursor='pointer'"
					onmouseout="this.style.cursor='default'">Nazwisko</td>
				<td
					onclick="document.location.href='/webapp_appoitment/?sortBy=dateOfBirth'"
					onmouseover="this.style.cursor='pointer'"
					onmouseout="this.style.cursor='default'">Data urodzenia</td>
			</tr>
			<tr>
				<c:forEach items="${patientsList}" var="e" varStatus="iteration">
					<tr>
						<td>${e.name}</td>
						<td>${e.surname}</td>
						<td><fmt:formatDate value="${e.dateOfBirth}"
								pattern="yyyy-MM-dd" /></td>
				</c:forEach>
			</tr>
		</table>
		<table style="width: 550px; margin-left: auto; margin-right: auto;"
			border="1" cellspacing="0" cellpadding="0">
			<tr bgcolor="orange">
				<td>Data</td>
				<td>Cena</td>
				<td>Pow�d</td>
				<td>Pacjent</td>
			</tr>
			<c:forEach items="${appointmentsList}" var="a">
				<tr>
					<td><fmt:formatDate value="${a.date}" pattern="yyyy-MM-dd" /></td>
					<td>${a.price}</td>
					<td>${a.reason}</td>
					<td>${a.patient.name}&nbsp;${a.patient.surname}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>
