<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Employee page</title>
</head>
<body>
	<div id="loginForm"
		style="background-color: #EEEEEE; margin-left: auto;">
		<c:if test="${appUser==null}">
			<form action="doLogin" method="post">
				<table style="margin-left: auto;">
					<tr>
						<td>Login</td>
						<td><input type="text" name="user_name" />
						<td>Haslo</td>
						<td><input type="password" name="password" />
						<td><input type="submit" value="zaloguj" /></td>
					</tr>
				</table>
			</form>
		</c:if>
		<c:if test="${appUser!=null}">
			<form action="doLogout" method="post">
				<table style="margin-left: auto;">
					<tr>
						<td>Witaj: ${appUser.name}</td>
						<td><input type="submit" value="Wyloguj" /></td>
					</tr>
				</table>
			</form>
		</c:if>
	</div>
	<div style="width: 600px; margin-left: auto; margin-right: auto;">
		<div id="addForm"
			style="width: 550px; margin-left: auto; margin-right: auto; background-color: #EEEEEE; border: 1px solid">
			<form action="addPatient" method="post">
				<table style="width: 500px; margin-left: auto; margin-right: auto;">
					<tr>
						<td>Wpisz imie</td>
						<td><input type="text" name="name" /> <c:if
								test="${error_name!=null}">
								<font color="red">${error_name}</font>
							</c:if></td>
					</tr>
					<tr>
						<td>Wpisz nazwisko</td>
						<td><input type="text" name="surname" />
						<c:if test="${error_surname!=null}">
								<font color="red">${error_surname}</font>
							</c:if></td>
					</tr>
					<tr>
						<td>Wpisz date urodzenia</td>
						<td><input type="text" name="dateOfBirth" />
						<c:if test="${error_dateOfBirth!=null}">
								<font color="red">${error_dateOfBirth}</font>
							</c:if></td>
					</tr>
					<tr>
						<td>Wpisz login</td>
						<td><input type="text" name="user_name" />
						<c:if test="${error_user_name!=null}">
								<font color="red">${error_user_name}</font>
							</c:if></td>
					</tr>
					<tr>
						<td>Wpisz haslo</td>
						<td><input type="text" name="password" />
						<c:if test="${error_password!=null}">
								<font color="red">${error_password}</font>
							</c:if></td>
					</tr>
					<tr>
						<td colspan="2" align="center"><input type="submit"
							value="Stworz konto" /></td>
					</tr>
				</table>
			</form>
		</div>

		<div id="resultPage"
			style="width: 550px; margin-left: auto; margin-right: auto; background-color: #EEEEEE; border: 1px solid">
			<table style="width: 550px; margin-left: auto; margin-right: auto;"
				border="1" cellspacing="0" cellpadding="0">
				<tr bgcolor="orange">
					<td>Imie</td>
					<td onclick="document.location.href='/webapp_appoitment/?sortBy=surname'"
						onmouseover="this.style.cursor='pointer'"
						onmouseout="this.style.cursor='default'">Nazwisko</td>
					<td onclick="document.location.href='/webapp_appoitment/?sortBy=dateOfBirth'"
						onmouseover="this.style.cursor='pointer'"
						onmouseout="this.style.cursor='default'">Data urodzenia</td>
				</tr>
				<c:forEach items="${patientsList}" var="e">
					<tr>
						<td>${e.name}</td>
						<td>${e.surname}</td>
						<td><fmt:formatDate value="${e.dateOfBirth}"
								pattern="yyyy-MM-dd" /></td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
</body>
</html>