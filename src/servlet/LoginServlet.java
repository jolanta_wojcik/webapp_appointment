package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Patient;
import service.PatientService;

@WebServlet("/doLogin")
public class LoginServlet extends HttpServlet{

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String login = request.getParameter("user_name");
		String password = request.getParameter("password");
		
		Patient user = null;
		try {
			user = new PatientService().findUser(login, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		request.getSession().setAttribute("appUser", user);
		
		if(user.getLogin().equals("admin")){
			response.sendRedirect("admin_profile");	
		}else{
			response.sendRedirect("user_profile");	
		}
	}
}
