package servlet;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Appointment;
import model.Patient;
import service.AppointmentService;

@WebServlet("/addAppointment")
public class AddAppointmentServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	private AppointmentService appService;

	@Override
	public void init() throws ServletException {
		appService = new AppointmentService();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		addAppointment(request);
		
		RequestDispatcher rd = request.getRequestDispatcher("/user_profile");
		rd.forward(request, response);
	}

	private void addAppointment(HttpServletRequest request) {
		Patient user = (Patient) request.getSession().getAttribute("appUser");
		try {
			appService.insertAppointment(new Appointment(-1, Date.valueOf(request.getParameter("dateOfApp")),
							Integer.valueOf(request.getParameter("price")), request.getParameter("reason"),
							user));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
