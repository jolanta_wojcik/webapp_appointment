package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Patient;
import service.AppointmentService;

@WebServlet("/user_profile")
public class UserServlet extends HttpServlet{
	private AppointmentService as;
	
	@Override
	public void init() throws ServletException {
		as = new AppointmentService();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			Patient user = (Patient) req.getSession().getAttribute("appUser");
			req.setAttribute("appointmentsList", as.queryPatientAppointment(user.getId()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		RequestDispatcher rd = req.getRequestDispatcher("user_profile.jsp");
		rd.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}
}
