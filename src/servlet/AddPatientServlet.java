package servlet;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Patient;
import service.PatientService;

@WebServlet("/addPatient")
public class AddPatientServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	private PatientService patientService;

	@Override
	public void init() throws ServletException {
		patientService = new PatientService();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		boolean validated = validateResult(request);
		if (validated) {
			addPatient(request);
		}
		RequestDispatcher rd = request.getRequestDispatcher("/");
		rd.forward(request, response);
	}

	private void addPatient(HttpServletRequest request) {
		try {
			patientService
					.insertPatient(new Patient(-1, request.getParameter("name"), request.getParameter("surname"),
							Date.valueOf(request.getParameter("dateOfBirth")), request.getParameter("user_name"), 
									request.getParameter("password")));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private boolean validateResult(HttpServletRequest request) {
		boolean result = true;
		if (request.getParameter("name") == null || request.getParameter("name").length() == 0) {
			result = false;
			request.setAttribute("error_name", "imie nie moze byc puste");
		}
		if (request.getParameter("surname") == null || request.getParameter("surname").length() == 0) {
			result = false;
			request.setAttribute("error_surname", "nazwisko nie moze byc puste");
		}
		if (request.getParameter("dateOfBirth") == null || request.getParameter("dateOfBirth").length() == 0) {
			result = false;
			request.setAttribute("error_dateOfBirth", "pole data urodzenia nie moze byc puste");
		}
		if (request.getParameter("user_name") == null || request.getParameter("user_name").length() == 0) {
			result = false;
			request.setAttribute("error_user_name", "user_name nie moze byc pusta");
		}
		if (request.getParameter("password") == null || request.getParameter("password").length() == 0) {
			result = false;
			request.setAttribute("error_password", "pole nie moze byc pusta");
		}
		return result;
	}
}
