package servlet;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Patient;
import service.AppointmentService;
import service.PatientService;

@WebServlet("/admin_profile")
public class AdminServlet extends HttpServlet{
	private AppointmentService as;
	private PatientService ps;
	
	@Override
	public void init() throws ServletException {
		as = new AppointmentService();
		ps = new PatientService();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			String sortBy = Optional.ofNullable(req.getParameter("sortBy")).orElse("id");
			req.setAttribute("patientsList", ps.queryPatient(sortBy));
			req.setAttribute("appointmentsList", as.queryAppointment());
		} catch (Exception e) {
			e.printStackTrace();
		}
		RequestDispatcher rd = req.getRequestDispatcher("admin_profile.jsp");
		rd.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}
}
