package service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.Patient;

public class PatientService extends DbService {

		private PreparedStatement insert;
		private PreparedStatement delete;

		private Connection connection;

		public PatientService() {
			try {
				this.connection = connection();
				this.insert = connection.prepareStatement(
						"insert into patient(name, surname, dateOfBirth, user_name, password) values (?,?,?,?,?)",
						PreparedStatement.RETURN_GENERATED_KEYS);
				this.delete = connection.prepareStatement("delete from patient where id = ?");
			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		}
		
		public Patient findUser(String userName, String pass) throws SQLException {
			// TODO: zamien user account na patient i dodaj do tabeli/klasy patient
			// kolumny: username, password
			StringBuilder sql = new StringBuilder("Select * from patient where user_name = ? and password= ?");

			PreparedStatement pstm = connection.prepareStatement(sql.toString());
			pstm.setString(1, userName);
			pstm.setString(2, pass);
			ResultSet rs = pstm.executeQuery();
			Patient user = null;

			if (rs.next()) {
				user = new Patient(rs.getInt("id"),
						rs.getString("name"),
						rs.getString("surname"),
						rs.getDate("dateOfBirth"),
						userName, pass);
			}
			rs.close();
			pstm.close();
			return user;
		}

		public List<Patient> queryPatient(String sortBy) throws SQLException, ParseException {
			PreparedStatement select = connection.prepareStatement("select * from patient order by " + sortBy);
			ResultSet rs = select.executeQuery();
			List<Patient> list = new ArrayList<Patient>();
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String surname = rs.getString("surname");
				String dateOfBirth = rs.getString("dateOfBirth");
				String user_name = rs.getString("user_name");
				String password = rs.getString("password");
				SimpleDateFormat sdt = new SimpleDateFormat("yyyy-MM-dd");
				Date date = (Date)sdt.parse(dateOfBirth); 
				list.add(new Patient(id, name, surname, sdt.parse(dateOfBirth), user_name, password));
			}
			rs.close();
			return list;
		}

		public void insertPatient(Patient pt) throws SQLException {
			
			insert.setString(1, pt.getName());
			insert.setString(2, pt.getSurname());
			insert.setDate(3, (java.sql.Date) pt.getDateOfBirth());
			insert.setString(4, pt.getLogin());
			insert.setString(5, pt.getPassword());
			insert.executeUpdate();
			
			ResultSet rs = insert.getGeneratedKeys();
			if (rs.next()) {
				pt.setId(rs.getInt("id"));
			}
			
			rs.close();
		}

		public void deletePatient(Patient pt) throws SQLException {
			delete.setInt(1, pt.getId());

			ResultSet rs = delete.executeQuery();
			rs.close();
			delete.close();
		}
}
