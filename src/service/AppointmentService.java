package service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.Appointment;
import model.Patient;

public class AppointmentService extends DbService {
	private static final SimpleDateFormat sdt = new SimpleDateFormat("yyyy-MM-dd");
	private PreparedStatement insert;
	private PreparedStatement delete;

	private Connection connection;

	public AppointmentService() {
		try {
			this.connection = connection();
			this.insert = connection.prepareStatement(
					"insert into appoitment(dateOfApp, price, reasone, patient_id) values (?,?,?,?)",
					PreparedStatement.RETURN_GENERATED_KEYS);
			this.delete = connection.prepareStatement("delete from Appointment where id = ?");
		} catch (Exception e) {
			throw new IllegalStateException(e);
		}
	}

	public List<Appointment> queryAppointment() throws SQLException, ParseException {
		PreparedStatement select = connection.prepareStatement("SELECT " + "*" + "FROM appoitment "
				+ "LEFT OUTER JOIN patient " + "ON appoitment.patient_id = patient.id ");
		ResultSet rs = select.executeQuery();
		List<Appointment> list = new ArrayList<Appointment>();
		while (rs.next()) {
			int id = rs.getInt("id");
			String date = rs.getString("dateOfApp");
			int price = rs.getInt("price");
			String reason = rs.getString("reasone");
			Patient patient = new Patient(rs.getInt("patient_id"), rs.getString("name"), rs.getString("surname"),
					rs.getDate("dateOfBirth"), rs.getString("user_name"), rs.getString("password"));
			list.add(new Appointment(id,(Date) sdt.parse(date), price, reason, patient));
		}
		rs.close();
		return list;
	}

	public List<Appointment> queryPatientAppointment(int patient_id) throws SQLException, ParseException {
		PreparedStatement select = connection
				.prepareStatement("SELECT " + "*" + "FROM appoitment " + "LEFT OUTER JOIN patient "
						+ "ON appoitment.patient_id = patient.id " + "where appoitment.patient_id = " + patient_id);
		ResultSet rs = select.executeQuery();
		List<Appointment> list = new ArrayList<Appointment>();
		while (rs.next()) {
			String date = rs.getString("dateOfApp");
			int price = rs.getInt("price");
			String reason = rs.getString("reasone");
			list.add(new Appointment((Date) sdt.parse(date), price, reason));
		}
		rs.close();
		return list;
	}

	public void insertAppointment(Appointment ap) throws SQLException {
		insert.setDate(1, (java.sql.Date) ap.getDate());
		insert.setInt(2, ap.getPrice());
		insert.setString(3, ap.getReason());
		insert.setInt(4, ap.getPatient().getId());
		insert.executeUpdate();
		ResultSet rs = insert.getGeneratedKeys();
		if (rs.next()) {
			ap.setId(rs.getInt(1));
		}
		rs.close();
	}
}
